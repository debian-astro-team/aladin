
import cds.aladin.*;
import cds.tools.*;

public class CubeManipulationPlug extends AladinPlugin implements VOObserver {
   
   public String menu() { return "Spectrum extraction from a cube"; }
   public String description() {
      return "PLUGIN TUTORIAL:\n" +
      "This plugin is an example a cube manipulation.\n" +
      "This plugin extracts the spectrum at the mouse position.";
   }   
   public String category()    { return "Plugin tutorial/Cube"; }
   public String version()     { return "1.0 - January 2007"; }
   public String author()      { return "Pierre Fernique [CDS]"; }
   public String url()         { return "http://aladin.u-strasbg.fr/java/Plugins/CubeManipulationPlug.java"; }
   
   // Register this plugin as a VOObserver (see position() method)
   public void exec() {
      aladin.addObserver(this,VOApp.POSITION);
   }
   
   // Call by Aladin when the user clicks on the mouse
   public void position(double ra, double dec) {
      try {
         AladinData ad = aladin.getAladinData();
         
         String planeType = ad.getPlaneType();
         if ( !(planeType.equals("Image/Cube")) ) {
             System.out.println("Only Image/Cube is supported");
             return;
         } 

         double xy[] = ad.getXY(ra,dec);
         int X = (int)(xy[0]-0.5);
         int Y = (int)(xy[1]-0.5);
         
         int depth = ad.getDepth();
         double cube[][][] = ad.getCube(X,Y,0, 1,1,depth);
         
         System.out.print("First 20th spectrum values at ("+X+","+Y+") : ");
         for( int i=0; i<Math.min(20,depth); i++ ) System.out.print(" "+cube[0][0][i]);
         System.out.println();
         
      } catch( AladinException e ) { e.printStackTrace(); }
   } 
   
   // Not used here
   public void pixel(double pixValue) {}
}