aladin (12.060+dfsg-3) unstable; urgency=medium

  * Convert to adql-2 Java package (Closes: #1058415).

 -- Ole Streicher <olebole@debian.org>  Wed, 13 Dec 2023 09:49:01 +0100

aladin (12.060+dfsg-2) unstable; urgency=medium

  [ Vladimir Petko ]
  * d/p/fully-qualify-builder.patch: fully qualify cds.allsky.Builder class to
    avoid name collision with java.lang.thread.Builder (Closes: #1052586).

 -- Ole Streicher <olebole@debian.org>  Mon, 20 Nov 2023 10:02:58 +0100

aladin (12.060+dfsg-1) unstable; urgency=medium

  * New upstream version 12.060+dfsg
  * Push Standards-Version to 4.6.2. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Mon, 26 Jun 2023 08:52:13 +0200

aladin (12.001+dfsg-1) unstable; urgency=medium

  * New upstream version 12.001+dfsg
  * Install old original build.xml
  * Push Standards-Version to 4.6.1. No changes needed

 -- Ole Streicher <olebole@debian.org>  Tue, 27 Sep 2022 19:21:09 +0200

aladin (11.024+dfsg2-1) unstable; urgency=low

  * Use Debian's jsofa instead of builtin copy
  * Add "Rules-Requires-Root: no" to d/control

 -- Ole Streicher <olebole@debian.org>  Thu, 02 Jul 2020 09:11:34 +0200

aladin (11.024+dfsg-1) unstable; urgency=low

  * Remove (wrong) base healpix dir (should be cds/healpix) in get-orig-source
  * New upstream version 11.024+dfsg

 -- Ole Streicher <olebole@debian.org>  Fri, 29 May 2020 09:08:15 +0200

aladin (11.021+dfsg-1) unstable; urgency=low

  * New upstream version 11.021+dfsg. Rediff patches
  * Push Standards-Version to 4.5.0. No changes needed.
  * Push compat to 13. Replace d/compat by debhelper-compat virtual build dep
  * Remove unneeded build dep versions
  * Use system package for cds/healpix
  * Add copyright for org.jastronomy.jsofa

 -- Ole Streicher <olebole@debian.org>  Sun, 03 May 2020 17:56:56 +0200

aladin (10.076+dfsg-1) unstable; urgency=low

  * New upstream version 10.076+dfsg
  * Reorganize patches a bit; adjust Don-t-call-home-for-logging.patch
  * Remove obsolete options from manpage and desktop entry
  * Update VCS fields to use salsa.d.o
  * Delete d/s/local-options
  * Push Standards-Version to 4.1.3. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Thu, 08 Feb 2018 15:37:53 +0100

aladin (10.056+dfsg-2) unstable; urgency=low

  * Use package provided moc, savot, and units
  * Add CDS as the creator of the software to the description
  * Update and extend manpage

 -- Ole Streicher <olebole@debian.org>  Wed, 27 Dec 2017 14:56:02 +0100

aladin (10.056+dfsg-1) unstable; urgency=low

  * New upstream version 10.056+dfsg. Rediff patches
  * Official upstream version: switch to unstable
  * Push Standards-Version to 4.1.2. Change URLs to https

 -- Ole Streicher <olebole@debian.org>  Thu, 07 Dec 2017 14:38:12 +0100

aladin (10.033+dfsg-1) experimental; urgency=low

  * Initial release. (Closes: #818373)

 -- Paul Sladen <debian@paul.sladen.org>  Mon, 23 Oct 2017 22:07:47 +0200
