// Copyright 2021 - Unistra/CNRS
// The MOC API project is distributed under the terms
// of the GNU General Public License version 3.
//
//This file is part of MOC API java project.
//
//    MOC API java project is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, version 3 of the License.
//
//    MOC API java project is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    The GNU General Public License is available in COPYING file
//    along with MOC API java project.
//

package cds.moc;

/** MOC cell object
 * 
 * @author Pierre Fernique [CDS]
 * @version 2.0 apr 2021 - refactoring (Moc 2.0)
 * @version 1.0 dec 2011 - creation
 */
public class MocCell {
   public char dim;             // Char signature of the dimension ('s' for SPACE, 't' for TIME...)
   public int order;            // Order of the Moc cell;
   public long start;           // Cell value, or start index for a Range
   public long end;             // Cell value+1, or end index (excluded) for a Range
   public Moc1D moc;            // Moc1D associated to the cell (ex: for STMoc)
}
